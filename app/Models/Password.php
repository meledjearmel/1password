<?php

namespace App\Models;

use Carbon\Carbon;
use App\Models\Secret;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Casts\AsArrayObject;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Password extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'title',
        'username',
        'password',
        'website',
        'comment',
        'used_at',
    ];

    protected $casts = [
        'used_at' => 'datetime',
        'website' => AsArrayObject::class,
    ];

    public static function create(array $attributes)
    {
        $attributes['used_at'] = Carbon::now();
        return parent::create($attributes);
    }

    public function secret ()
    {
        return $this->belongsTo(Secret::class);
    }
}
