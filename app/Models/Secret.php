<?php

namespace App\Models;

use App\Models\Note;
use App\Models\User;
use App\Models\Identity;
use App\Models\Password;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Secret extends Model
{
    use HasFactory, SoftDeletes;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function passwords()
    {
        return $this->hasMany(Password::class);
    }

    public function notes()
    {
        return $this->hasMany(Note::class);
    }

    public function identities()
    {
        return $this->hasMany(Identity::class);
    }
}
