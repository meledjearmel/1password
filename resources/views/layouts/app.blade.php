<html lang="fr">
<head>
    <title>1Password pour Armel Meledje</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="referrer" content="no-referrer">
    <link rel="stylesheet" media="all" href="{{ asset('css/app.1password.css') }}" crossorigin="anonymous">

    <link rel="shortcut icon" type="image/x-icon" href="https://app.1password.com/images/favicon.ico">
    <link rel="apple-touch-icon-precomposed" href="https://app.1password.com/images/apple-touch-icon-ipad.png" sizes="76x76">
    <link rel="apple-touch-icon-precomposed" href="https://app.1password.com/images/apple-touch-icon-iphone.png" sizes="60x60">
    <link rel="apple-touch-icon-precomposed" href="https://app.1password.com/images/apple-touch-icon-iphone-3x.png" sizes="180x180">
    <link rel="apple-touch-icon-precomposed" href="https://app.1password.com/images/apple-touch-icon-ipad-pro.png" sizes="167x167">
    <link rel="apple-touch-icon-precomposed" href="https://app.1password.com/images/apple-touch-icon-ipad-2x.png" sizes="152x152">
    <link rel="apple-touch-icon-precomposed" href="https://app.1password.com/images/apple-touch-icon-iphone-2x.png" sizes="120x120">
    <link rel="mask-icon" href="https://app.1password.com/images/pinned-tab.svg" color="black">
</head>

<body class="clickup-chrome-ext_installed vault-view">

    <div id="b5app">
        <div class="full-page" aria-hidden="false">
            @yield('content')
        </div>
    </div>

    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
