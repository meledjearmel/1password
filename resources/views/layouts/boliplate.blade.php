@extends('layouts.app')

@section('content')
    <div id="unlocked-app">
        <header id="top-bar">
            <div id="title-container" tabindex="-1" aria-label="" class="title-container--top_bar_K1eHp">
                <a class="link--link_ejvK7 main-icon--top_bar_dE4C1" id="main-icon" aria-label="Accueil" href="/dashboard">
                    <img src="{{ asset('img/1password-icon.svg') }}" width="46" height="46" alt="" role="presentation">
                </a>
                <div class="menu-wrapper--menu_QHZkx menu-wrapper" id="vault-switcher" data-testid="menu">
                    <button class="menu-button--menu_EfvjT" aria-haspopup="true" aria-expanded="false" aria-label="Privé" data-testid="menu-button">
                        <div>
                            <div aria-hidden="true" class="avatar vault small">
                                <img alt="Avatar" src="https://a.1passwordusercontent.com/JSTHBVZ5IRABRKU76CDIDT5RBY/pmeffwrezbdvpgcdbt6unmt4rm.png">
                            </div>
                            <h1>Privé</h1>
                        </div>
                    </button>
                </div>
            </div>
            <div id="user-menu" class="user-menu--top_bar_CVH8i hide--top_bar_t2Vgq">
                <div class="menu-wrapper--menu_QHZkx menu-container" data-testid="user-menu">
                    <button class="menu-button--menu_EfvjT" aria-haspopup="true" aria-expanded="false" aria-label="Menu utilisateur" data-testid="user-menu-button">
                        <div>
                            <div aria-hidden="true" class="avatar default small">
                                <img alt="Avatar" src="https://a.1passwordusercontent.com/JSTHBVZ5IRABRKU76CDIDT5RBY/pmeffwrezbdvpgcdbt6unmt4rm.png">
                            </div>
                            <label class="account-name">Armel Meledje</label>
                            <img class="menu-icon" src="https://app.1password.com/images/dropdown-chevron-v2.svg" alt="" width="12px" height="7px">
                        </div>
                    </button>
                </div>
            </div>
        </header>

        <section id="vault-content" data-testid="vault-ready"><a id="main-content"></a>
            <nav id="sidebar">
                <ul>
                    <li class="selected">
                        <button class="sidebar-list-button ">
                            <img src="{{ asset('img/all-elements.svg') }}" width="18">
                            <span class="name">Tous les éléments</span>
                            <span data-testid="allItems-count" class="count">49</span>
                        </button>
                    </li>

                    <div class="sidebar-section-header">
                        <h2>Catégories</h2><span>Masquer</span>
                    </div>

                    <li class="">
                        <a class="sidebar-list-button" href="#">
                            <img src="{{ asset('img/connexion-small.svg') }}" width="20">
                            <span class="name">Identifiants</span>
                        </a>
                    </li>
                    <li class="">
                        <a class="sidebar-list-button" href="#">
                            <img src="{{ asset('img/note-small.svg') }}" width="20">
                            <span class="name">Notes sécurisées</span>
                        </a>
                    </li>
                    <li class="">
                        <a class="sidebar-list-button" href="#">
                            <img src="{{ asset('img/identite-small.svg') }}" width="20">
                            <span class="name">Identités</span>
                        </a>
                    </li>

                    <div class="sidebar-section-header">
                        <h2>Tags</h2><span>Masquer</span>
                    </div>

                    <li>
                        <button class="sidebar-list-button">
                            <img src="{{ asset('img/tags-small.svg') }}" width="20">
                            <span class="name">Starter Kit</span>
                        </button>
                    </li>

                    <div class="archive">
                        <li class="">
                            <button class="sidebar-list-button">
                                <img src="{{ asset('img/corbeille-small.svg') }}" width="18">
                                <span class="name">Corbeille</span>
                                <span class="count">1</span>
                            </button>
                        </li>
                    </div>
                </ul>
            </nav>

            <div>
                <div id="search">
                    <input type="search" class="short" autocapitalize="none" placeholder="Rechercher" autocorrect="off" autocomplete="off" spellcheck="false" value="">
                </div>
                <div id="item-list">
                    <ul>
                        <li data-testid="vault-list-item">
                            <button class="list-item template-001" id="_dkpss4w555ql5juzbonjsr77nu-3otcqqj23kwq6vph7jaw5tp734" tabindex="0">
                                <img alt="" class="icon small left" width="32" src="{{ asset('img/connexion.png') }}" role="presentation">
                                <h4 title="[Dev] Myswissapple">[Dev] Myswissapple</h4>
                                <small title="Support">Support</small>
                            </button>
                        </li>
                        <li data-testid="vault-list-item">
                            <button class="list-item template-003" id="_dkpss4w555ql5juzbonjsr77nu-woug5vf32qe6t2o2kgykpe55fy" tabindex="0">
                                <img alt="" class="icon small left" width="32" src="https://a.1passwordusercontent.com/template/003.png" role="presentation">
                                <h4 title="🎉 Bienvenue sur 1Password!">🎉 Bienvenue sur 1Password!</h4>
                                <small title="Suivez ces étapes pour commencer.">Suivez ces étapes pour commencer.</small>
                            </button>
                        </li>
                        <li data-testid="vault-list-item">
                            <button class="list-item template-004" id="_dkpss4w555ql5juzbonjsr77nu-h22s3nnlw73kmv64iafznekjcq" tabindex="0">
                                <img alt="" class="icon small left" width="32" src="https://a.1passwordusercontent.com/template/004.png" role="presentation">
                                <h4 title="Armel Meledje">Armel Meledje</h4>
                                <small title="Armel Meledje">Armel Meledje</small>
                            </button>
                        </li>
                    </ul>
                </div>
            </div>

            <section id="item-details" class="">
                @yield('section')
            </section>
        </section>
    </div>
@endsection
