@extends('layouts.boliplate')

@section('section')
    <header data-icon="">
        <div class="dropzone " tabindex="0" role="button">
            <img alt="" class="details-login-icon template-001" width="60" src="{{ asset('img/connexion.png') }}" role="presentation">
        </div>
        <h1>
            <input type="text" class="textfield--textfield_u2NGp" placeholder="Titre" autocapitalize="words" autocorrect="off" autocomplete="off" spellcheck="false" value="">
        </h1>
    </header>

    <table class="section-username-password">
        <tbody>
            <tr class="field string" data-k="string" data-n="" data-i="{&quot;autocorrection&quot;:&quot;no&quot;}">
                <td class="title first-col">
                    <input type="text" class="textfield--textfield_u2NGp" autocapitalize="none" placeholder="étiquette" readonly="" tabindex="-1" autocorrect="off" autocomplete="off" spellcheck="false" value="Nom d'utilisateur">
                </td>
                <td class="value string " data-copy-text="">
                    <input type="text" class="value-input" autocapitalize="none" autocorrect="off" placeholder="nouveau champ" autocomplete="off" spellcheck="false" value="">
                    <div class="change-field-type-container"></div>
                </td>
            </tr>
            <tr class="field concealed" data-k="concealed" data-n="" data-i="{&quot;autocorrection&quot;:&quot;no&quot;}">
                <td class="title first-col">
                    <input type="text" class="textfield--textfield_u2NGp" autocapitalize="none" placeholder="étiquette" readonly="" tabindex="-1" autocorrect="off" autocomplete="off" spellcheck="false" value="mot de passe">
                </td>
                <td class="value concealed show-password-generator" data-copy-text="">
                    <input type="text" class="value-input" data-value="" data-entropy="0" data-generator-used="false" autocapitalize="none" autocorrect="off" autocomplete="off" spellcheck="false" value="••••••••••">
                    <button class="generate-password">
                        <img src="https://app.1password.com/images/password-generator.svg" alt="Générer un mot de passe" role="button">
                    </button>
                    <div class="change-field-type-container"></div>
                </td>
            </tr>
        </tbody>
    </table>

    <table class="section-websites">
        <tbody>
            <tr class="field URL new-field" data-k="URL" data-n="" data-i="{&quot;autocorrection&quot;:&quot;no&quot;}">
                <td class="title first-col">
                    <input type="text" class="textfield--textfield_u2NGp" autocapitalize="none" placeholder="site web" autocorrect="off" autocomplete="off" spellcheck="false" value="">
                </td>
                <td class="value URL" data-copy-text="">
                    <input type="text" class="value-input" autocapitalize="none" autocorrect="off" placeholder="adresse web" autocomplete="off" spellcheck="false" value="">
                    <div class="change-field-type-container"></div>
                </td>
            </tr>
        </tbody>
    </table>

    <table class="section-notes">
        <tbody>
            <tr>
                <td class="first-col">remarques</td>
                <td class="notes">
                    <textarea placeholder=" " autocorrect="off" autocomplete="off" spellcheck="false" style="height: 100px;"></textarea>
                </td>
            </tr>
        </tbody>
    </table>
    
    <section data-testid="toolbar" class="toolbar--toolbar_ZG8Vl">
        <div class="left--toolbar_KCPq2"></div>
        <div class="right--toolbar_Db2ID">
            <button class="save--toolbar_ZBdqy" data-testid="toolbar-save">Enregistrer</button>
            <button class="cancel--toolbar_SP34T" data-testid="toolbar-cancel">Annuler</button>
        </div>
    </section>
@endsection
