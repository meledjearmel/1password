@extends('layouts.app')

@section('content')
    <div id="signup">
        <div class="header-container">
            <img src="https://app.1password.com/images/1password-icon-ca60f9cda8fbce72a8ba.svg" class="main-icon" width="46" height="46" alt="1Password">
        </div>
        <div class="main-container">
            <main class="start--index_juHOh">
                <section class="form--index_eGaVE">
                    <div class="popout">
                        <h1>Créer un compte</h1>
                        <form role="form" aria-label="Créer un compte">
                            <div class="section">
                                <label for="user-name">Nom</label>
                                <input autocapitalize="words" autocomplete="name" autocorrect="off" class="" id="user-name" maxlength="60" spellcheck="false" type="text" value="">
                            </div>
                            <div class="section">
                                <label for="email">Adresse email</label>
                                <input autocapitalize="none" autocomplete="email" autocorrect="off" class="" id="email" maxlength="255" spellcheck="false" type="email" value="">
                            </div>
                            <button id="submit" class="next-button--index_sCs0T new-button blue" aria-describedby="terms-and-conditions">Créer un compte</button>
                            <p id="terms-and-conditions" class="terms--index_oT7rh">En continuant, vous acceptez les <a target="_blank" rel="noreferrer" href="https://1password.com/legal/terms-of-service/">conditions d'utilisation</a> et l'<a target="_blank" rel="noreferrer" href="https://1password.com/legal/privacy/">avis de confidentialité</a>.</p>
                        </form>
                    </div>
                </section>
                <section class="text-gray-bg features--index_lrnyT">
                    <h1>Ciapol 1Password</h1>
                    <ul>
                        <li><img src="https://app.1password.com/images/individual-checkmark-de0c9182d89619c95823.svg" alt="" role="presentation">N'oubliez plus vos mot de passe.</li>
                        <li><img src="https://app.1password.com/images/individual-checkmark-de0c9182d89619c95823.svg" alt="" role="presentation">Creez votre compte.</li>
                        <li><img src="https://app.1password.com/images/individual-checkmark-de0c9182d89619c95823.svg" alt="" role="presentation">Ajoutez vos mot de passe.</li>
                        <li><img src="https://app.1password.com/images/individual-checkmark-de0c9182d89619c95823.svg" alt="" role="presentation">Retrouvez les quand vous le souhaitez.</li>
                    </ul>
                </section>
            </main>
        </div>
        <div class="background">
            <img class="bottom-left" src="https://app.1password.com/images/individual-bottom-left-d373ce354fcaebb02c93.svg" alt="" role="presentation">
            <img class="top-right" src="https://app.1password.com/images/individual-top-right-b73dfaf148b3af00de3c.svg" alt="" role="presentation">
        </div>
    </div>
@endsection
