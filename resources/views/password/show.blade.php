@extends('layouts.boliplate')

@section('content')
<header data-icon="">
    <img alt="" class="details-login-icon template-001" width="60" src="{{ asset('img/connexion.png') }}" role="presentation">
    <h1>[Dev] Myswissapple</h1>
    <div class="actions">
        <button class="favorite-button" aria-label="article préféré"></button>
        <button class="send-to-button" aria-label="share item"></button>
    </div>
</header>
<table class="section-username-password">
    <tbody>
        <tr class="field string" data-k="string" data-n="" data-i="{&quot;autocorrection&quot;:&quot;no&quot;}">
            <td class="title first-col">
                <span>Nom d'utilisateur</span>
            </td>
            <td class="value string " data-copy-text="Support">
                <div class="value-container">
                    <span>Support</span>
                    <button class="field-button" aria-label="Copier Nom d'utilisateur">Copier</button>
                </div>
                <div class="change-field-type-container"></div>
            </td>
        </tr>
        <tr class="field concealed" data-k="concealed" data-n="" data-i="{&quot;autocorrection&quot;:&quot;no&quot;}">
            <td class="title first-col">
                <span>mot de passe</span>
            </td>
            <td class="value concealed show-password-generator" data-copy-text="@n@!UI)yqNzr$NeN$W$wDOsU">
                <div class="value-container">
                    <span>
                        <span class="symbol">@</span>
                        <span class="letter">n</span>
                        <span class="symbol">@</span>
                        <span class="symbol">!</span>
                        <span class="letter">U</span>
                        <span class="letter">I</span>
                        <span class="symbol">)</span>
                        <span class="letter">y</span>
                        <span class="letter">q</span>
                        <span class="letter">N</span>
                        <span class="letter">z</span>
                        <span class="letter">r</span>
                        <span class="symbol">$</span>
                        <span class="letter">N</span>
                        <span class="letter">e</span>
                        <span class="letter">N</span>
                        <span class="symbol">$</span>
                        <span class="letter">W</span>
                        <span class="symbol">$</span>
                        <span class="letter">w</span>
                        <span class="letter">D</span>
                        <span class="letter">O</span>
                        <span class="letter">s</span>
                        <span class="letter">U</span>
                    </span>
                    <button class="field-button" aria-label="Copier mot de passe">Copier</button>
                    <button class="field-button" aria-label="Révéler mot de passe">Révéler</button>
                    <button class="field-button" aria-haspopup="dialog" aria-label="Gros caractères mot de passe">Gros caractères</button>
                </div>
            </td>
        </tr>
    </tbody>
</table>
<table class="section-websites">
    <tbody>
        <tr class="field URL" data-k="URL" data-n="" data-i="{&quot;autocorrection&quot;:&quot;no&quot;}">
            <td class="title first-col">
                <span>site web</span>
            </td>
            <td class="value URL" data-copy-text="https://alexweboff-dev-my-swiss-apple.pf22.wpserveur.net/">
                <div class="value-container link">
                    <a class="link url" href="https://alexweboff-dev-my-swiss-apple.pf22.wpserveur.net/" title="https://alexweboff-dev-my-swiss-apple.pf22.wpserveur.net/" rel="noreferrer" target="_blank">
                        <span>https://<span class="domain">alexweboff-dev-my-swiss-apple.pf22.wpserveur.net</span>/</span>
                    </a>
                    <a class="field-button" href="https://alexweboff-dev-my-swiss-apple.pf22.wpserveur.net/" rel="noreferrer" target="_blank">Ouvrir</a>
                    <button class="field-button" aria-label="Copier site web">Copier</button>
                </div>
            </td>
        </tr>
    </tbody>
</table>
<footer>
    <table>
        <tbody>
            <tr>
                <td class="first-col">dernière modification</td>
                <td>21 juillet 2021 13:47</td>
            </tr>
            <tr>
                <td class="first-col">créé</td>
                <td>21 juillet 2021 13:43</td>
            </tr>
        </tbody>
    </table>
</footer>
<section data-testid="toolbar" class="toolbar--toolbar_ZG8Vl">
    <div class="left--toolbar_KCPq2">
        <button class="add--toolbar_CJojy" data-testid="toolbar-add" aria-label="créer un nouvel élément dans le coffre">+</button>
    </div>
    <div class="right--toolbar_Db2ID">
        <button class="edit--toolbar_PA8u6" data-testid="toolbar-edit">Éditer</button>
    </div>
</section>
@endsection
